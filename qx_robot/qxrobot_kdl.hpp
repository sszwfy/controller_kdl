﻿/**
 * \mainpage 琦星机器人KDL库
 *
 * \section intro_sec 基本说明
 *
 * 此库提供了机械臂运动算法、坐标转换算法的API
 *
 * \section install_sec 安装说明
 * 执行相关的cmake
 * \code
 * mkdir build
 * cd build
 * cmake .. 
 * \endcode
 */
/**
 * \file qxrobot_kdl.hpp
 * \brief KDL API接口
 *
*/

#ifndef QXROBOT_KDL_H
#define QXROBOT_KDL_H
#include "qxrobot_models.hpp"
#include <chainfksolverpos_recursive.hpp>
#include <stdint.h>

namespace KDL {
/**
  	* \brief Frame epsilon
  	*/
#define EPS_CALIBATION_REF_FRAME 1e-6

/**
  	* \brief 定义了相关的错误
  	*/
typedef enum {
    KDL_SUCCESS = 0,
    KDL_JNT_ARRAY_ERROR_NUM = -1,  //输入的JntArray参数关节数量 != Joint_num
    KDL_IK_INTERNAL_FAILD = -2,    //逆解内部错误
    KDL_FK_INTERNAL_FAILD = -3,    //正解内部错误
    KDL_KINEMATIC_FLAG_ERROR = -4, //ik_flag error or fk_flag error
    KDL_LIMIT_H_OFFSET = -10,      //positive limit base offset, -10标示关节0正向限制，-11标示关节1正向限制
    KDL_LIMIT_L_OFFSET = -20,      //negetive limit base offset，-20标示关节0负向限制，-21标示关节1负向限制
} KDL_ERROR;

/**
	* \brief 机械臂模型
	*/
typedef enum {
    QX_SCARA_V1 = 0,
    QX_A6_V1 = 1,
} QX_ROBOT_MODELS;

/**
	* \brief 四元数结构体
	*/
typedef struct {
    double x, y, z, w;
} Quaternion;

/**
	* \brief RPY结构体
	*/
typedef struct {
    double roll, pitch, yaw;
} RPY;

/**
	* \brief 机械臂KDL类
	*/
class QX_ROBOT_KDL {
  public:
    /**
		* \brief kdl_robot_kdl 构造函数
		* 该函数默认设置了机械臂的关节限制和DH参数，可通过设置函数来修改
		* \param [in] model_t 机械臂模型
		* \return kdl class
		*/
    QX_ROBOT_KDL(QX_ROBOT_MODELS model_t);
    ~QX_ROBOT_KDL() {
    }

    //--------------------------运动学函数---------------------
    /**
		* \brief 手掌正解
		* \param [in] joint 关节位置
		* \param [out] frame 正解结果
		* \param [out] fk_flag 正解的fk_flag
		* \return 参考 KDL_ERROR
		*/
    int32_t FkJointArray2FrameHand(const JntArray *joint, Frame *frame, int32_t *fk_flag);
    /**
		* \brief 手掌逆解
		* \param [in] last_posit 当前机械臂位置,SCARA机械臂，可以为NULL
		* \param [in] frame 关节位姿
		* \param [in] ik_flag
		* \param [out] joint 逆解的关节位置
		* \return 参考 KDL_ERROR
		*/
    int32_t IkFrame2JointArrayHand(const JntArray *last_posit, const Frame *frame, int32_t ik_flag, JntArray *joint);
    /**
		* \brief eef正解
		* \param [in] joint 关节位置
		* \param [out] frame 正解结果
		* \param [out] fk_flag 正解的fk_flag
		* \return 参考 KDL_ERROR
		*/
    int32_t FkJointArray2FrameEndEffector(const JntArray *joint, Frame *frame, int *fk_flag);
    /**
		* \brief eef逆解
		* \param [in] last_posit 当前机械臂位置,SCARA机械臂，可以为NULL
		* \param [in] frame 关节位姿
		* \param [in] ik_flag
		* \param [out] joint 逆解的关节位置
		* \return 参考 KDL_ERROR
		*/
    int32_t IkFrame2JointArrayEndEffector(const JntArray *last_posit, const Frame *frame, int32_t ik_flag, JntArray *joint);
    /**
		* \brief 设置end effector
		* \param [in] frame end effector的描述信息
		* \return 参考 KDL_ERROR
		*/
    int32_t SetEndEffector(const Frame *frame);
    /**
		* \brief 设置Joint位置限制
		* \param [in] limit_h 正限位
		* \param [in] limit_l 负限位
		* \return 参考 KDL_ERROR
		*/
    int32_t SetJointLimit(const JntArray *limit_h, const JntArray *limit_l);

    //-------------------------姿态描述转换函数-----------------
    /**
		* \brief 设置Joint位置限制
		* \param [in] limit_h 正限位
		* \param [in] limit_l 负限位
		* \return 参考 KDL_ERROR
		*/
    void Frame2Quaternion(const Frame *frame, Quaternion *quaternion_t); //转换frame的姿态矩阵到四元数
    /**
		* \brief 设置Joint位置限制
		* \param [in] limit_h 正限位
		* \param [in] limit_l 负限位
		* \return 参考 KDL_ERROR
		*/
    void Frame2Rpy(const Frame *frame, RPY *rpy_t); //转换frame的姿态矩阵到RPY角

    //-----------------------------坐标系转换函数-----------------
    /**
		* \brief 根据ref_frame将ref_pose转换world
		* \param [in] ref_pose 参考坐标系位姿
		* \param [in] ref_frame 坐标转换参数
		* \return KDL_SUCCESS: 转换成功
		*/
    Frame RefPose2World(const Frame *ref_pose, const Frame *ref_frame); //世界坐标系的位置转换到参考坐标系
    /**
		* \brief 根据ref_frame将world_pose转换ref
		* \param [in] world_pose 世界坐标系位姿
		* \param [in] ref_frame 坐标转换参数
		* \return KDL_SUCCESS: 转换成功
		*/
    Frame WorldPose2Ref(const Frame *world_pose, const Frame *ref_frame); //参考坐标系的位置转换到世界坐标系
    /**
		* \brief 计算1点坐标系标定
		* \param [in] point1 点1(原点)
		* \return KDL_SUCCESS: 成功
		*/
    int32_t CalcRefOnePoint(Frame *ref, const Frame *point1); //单点示教参考坐标系
    /**
		* \brief 计算2点带原点坐标系标定
		* \param [in] point1 点1(原点)
		* \param [in] Point2 点2
		* \return -1: point1和point2太近
		* \return KDL_SUCCESS: 成功
		*/
    int32_t CalcRefTwoPointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2); //两点带原点示教参考坐标系
    /**
		* \brief 计算2点无原点坐标系标定
		* \param [in] point1 点1
		* \param [in] Point2 点2
		* \return -1: point1和point2太近
		* \return KDL_SUCCESS: 成功
		*/
    int32_t CalcRefTwoPointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2); //两点带无原点示教参考坐标系
    /**
		* \brief 计算三点带原点坐标系标定
		* \param [in] point1 点1（原点）
		* \param [in] Point2 点2
		* \param [in] Point3 点3
		* \return -1: point1和point2太近
		* \return -2: point2和point3太近
		* \return -3: 三点共线
		* \return KDL_SUCCESS: 成功
		*/
    int32_t CalcRefThreePointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3); //三点带原点示教参考坐标系
    /**
		* \brief 计算三点无原点坐标系标定
		* \param [in] point1 点1
		* \param [in] Point2 点2
		* \param [in] Point3 点3
		* \return -1: point1和point2太近
		* \return -2: point2和point3太近
		* \return -3: 三点共线
		* \return KDL_SUCCESS: 成功
		*/
    int32_t CalcRefThreePointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3); //三点无原点示教参考坐标系

    //--------------------------Check函数------------------------
    /**
		* \brief 检查关节位置限制
		* \param [in] posit 关节位置
		* \return 参考 KDL_ERROR, 关节超过限制返回相应错误
		*/
    int32_t CheckJointLimit(const JntArray *posit); //检测关节限制
    /**
		* \brief 检测JntArray参数是否正确
		* \param [in] Jnt JntArray类型参数
		* \return 参考 KDL_ERROR
		*/
    int32_t CheckJntArray(const JntArray *Jnt); //检测JntArray参数是否正确
    /**
		* \brief 检查Kinematic flag
		* \param [in] kinematic_flag 
		* \return 参考 KDL_ERROR
		*/
    int32_t CheckKinematicFlag(int32_t kinematic_flag); //检测kinematic flag

    //-----------------------查询类------------------------------
    /**
		* \brief 获取关节数量
		* \param 无
		* \return 关节数量
		*/
    uint8_t GetJointNum() {
        return joint_num;
    }
    /**
		* \brief 获取qx_robot_kdl的版本号
		* \param 无
		* \return version
		*/
    std::string GetVersion();

    /**
		* \brief 查询关节限制
		* \param [in] kinematic_flag 
		* \return 参考 KDL_ERROR
		*/
    void GetJointLimit(JntArray *joint_limit_h, JntArray *joint_limit_l) {
        *joint_limit_h = posit_limit_h;
        *joint_limit_l = posit_limit_l;
        return;
    }

  private:
    uint8_t joint_num;
    JntArray posit_limit_h;
    JntArray posit_limit_l;
    Chain robot_t;
    QX_ROBOT_MODELS model;
    ChainFkSolverPos_recursive *fksolver;
    QxModels qx_models;
    Frame end_effector_forward;
    Frame end_effector_inverse;
    void CreatQxScaraV1(STRUCT_DH *dh);
};
}

#endif // !QXROBOT_KDL_H
