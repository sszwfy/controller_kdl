#include "qxrobot_kdl.hpp"
#include "qxrobot_models.hpp"
#include <chain.hpp>
#include <chainfksolverpos_recursive.hpp>
#include <chainidsolver_recursive_newton_euler.hpp>
#include <frames_io.hpp>
#include <kinfam_io.hpp>
#include <stdio.h>

using namespace KDL;

QX_ROBOT_KDL::QX_ROBOT_KDL(QX_ROBOT_MODELS model_t) {
    Frame frame;
    frame.p = Vector(0, 0, 0);
    //frame.M = Rotation(Vector(0, 0, 0), Vector(0, 0, 0), Vector(0, 0, 0));
    frame.M = Rotation::RPY(0, 0, 0);
    SetEndEffector(&frame);
    model = model_t;
    if (model == QX_SCARA_V1) {
        CreatQxScaraV1(NULL);
    } else {
        CreatQxScaraV1(NULL);
    }
    fksolver = new ChainFkSolverPos_recursive(robot_t);
    return;
}

void QX_ROBOT_KDL::CreatQxScaraV1(STRUCT_DH *dh) {
    robot_t = qx_models.QxScaraV1_Creat(dh);
    joint_num = (uint8_t) robot_t.getNrOfJoints();
    posit_limit_h = JntArray(joint_num);
    posit_limit_l = JntArray(joint_num);
    posit_limit_h.data << 2.8, 2.6, 150, KIN_PI;
    posit_limit_l.data << -2.8, -2.6, -150, -KIN_PI;
    return;
}

int32_t QX_ROBOT_KDL::SetEndEffector(const Frame *frame) {
    end_effector_forward = *frame;
    end_effector_inverse = end_effector_forward.Inverse();
    return 0;
}

int32_t QX_ROBOT_KDL::FkJointArray2FrameHand(const JntArray *joint, Frame *frame, int32_t *fk_flag) {
    int32_t ret;
    ret = CheckJntArray(joint);
    if (ret != KDL_SUCCESS) return ret;
    ret = CheckJointLimit(joint);
    if (ret != KDL_SUCCESS) return ret;
    ret = fksolver->JntToCart(*joint, *frame);
    if (ret != 0) return KDL_FK_INTERNAL_FAILD;
    if (fk_flag != NULL) {
        if (model == QX_SCARA_V1) qx_models.QxScaraV1_CheckArm(joint, fk_flag);
    }
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::IkFrame2JointArrayHand(const JntArray *last_posit, const Frame *frame, int32_t ik_flag, JntArray *joint) {
    int32_t ret = 0;
    *joint = JntArray(joint_num);
    ret = CheckKinematicFlag(ik_flag);
    if (ret != KDL_SUCCESS) return ret;
    JntArray inv_joint[8];
    for (int32_t i = 0; i < 8; i++) {
        inv_joint[i] = JntArray(joint_num);
    }
    if (model == QX_SCARA_V1) {
        ret = qx_models.QxScaraV1_InvKin(robot_t, frame, inv_joint);
        if (ret != 0) return KDL_IK_INTERNAL_FAILD;
        *joint = inv_joint[ik_flag];
    }
    ret = CheckJointLimit(joint);
    if (ret != KDL_SUCCESS) return ret;
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::FkJointArray2FrameEndEffector(const JntArray *joint, Frame *frame, int32_t *fk_flag) {
    Frame frame_hand;
    int32_t ret = 0;
    ret = FkJointArray2FrameHand(joint, &frame_hand, fk_flag);
    *frame = frame_hand * end_effector_forward;
    return ret;
}

int32_t QX_ROBOT_KDL::IkFrame2JointArrayEndEffector(const JntArray *last_posit, const Frame *frame, int32_t ik_flag, JntArray *joint) {
    Frame frame_hand;
    int32_t ret;
    frame_hand = *frame * end_effector_inverse;
    ret = IkFrame2JointArrayHand(last_posit, &frame_hand, ik_flag, joint);
    return ret;
}

void QX_ROBOT_KDL::Frame2Quaternion(const Frame *frame, Quaternion *quaternion_t) {
    frame->M.GetQuaternion(quaternion_t->x, quaternion_t->y, quaternion_t->z, quaternion_t->w);
    return;
}

void QX_ROBOT_KDL::Frame2Rpy(const Frame *frame, RPY *rpy_t) {
    frame->M.GetRPY(rpy_t->roll, rpy_t->pitch, rpy_t->yaw);
    return;
}

Frame QX_ROBOT_KDL::RefPose2World(const Frame *ref_pose, const Frame *ref_frame) {
    return (*ref_frame) * (*ref_pose);
}

Frame QX_ROBOT_KDL::WorldPose2Ref(const Frame *world_pose, const Frame *ref_frame) {
    return (ref_frame->Inverse()) * (*world_pose);
}

int32_t QX_ROBOT_KDL::CalcRefOnePoint(Frame *ref, const Frame *point1) {
    ref->M = Rotation::RPY(0, 0, 0);
    ref->p = point1->p;
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CalcRefTwoPointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2) {
    ref->p = point2->p - point1->p;
    ref->p.z(point1->p.z());
    Vector transl = ref->p;
    transl.z(0);
    if (transl.Norm() < EPS_CALIBATION_REF_FRAME) {
        return -1;
    }
    ref->p = point1->p;
    double theta = atan2(transl.y(), transl.x());
    ref->M = Rotation::RPY(0, 0, theta);
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CalcRefTwoPointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2) {
    ref->p = point2->p - point1->p;
    ref->p.z(0);
    if (ref->p.Norm() < EPS_CALIBATION_REF_FRAME) return -1;
    double theta = atan2(ref->p.y(), ref->p.x());
    ref->p = Vector(0, 0, 0);
    ref->M = Rotation::RPY(0, 0, theta);
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CalcRefThreePointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3) {
    Vector coor_x = point2->p - point1->p;
    Vector temp1 = point3->p - point1->p;
    if (coor_x.Norm() < EPS_CALIBATION_REF_FRAME) return -1;
    if (temp1.Norm() < EPS_CALIBATION_REF_FRAME) return -2;
    coor_x.Normalize();
    temp1.Normalize();
    if (coor_x == temp1) {
        return -3;
    }
    Vector coor_z = coor_x * temp1;
    Vector coor_y = coor_z * coor_x;
    coor_z.Normalize();
    coor_y.Normalize();
    ref->p = point1->p;
    ref->M = Rotation(coor_x, coor_y, coor_z);
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CalcRefThreePointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3) {
    Vector coor_x = point2->p - point1->p;
    Vector temp1 = point3->p - point1->p;
    if (coor_x.Norm() < EPS_CALIBATION_REF_FRAME) return -1;
    if (temp1.Norm() < EPS_CALIBATION_REF_FRAME) return -2;
    coor_x.Normalize();
    temp1.Normalize();
    if (coor_x == temp1) {
        return -3;
    }
    Vector coor_z = coor_x * temp1;
    Vector coor_y = coor_z * coor_x;
    coor_z.Normalize();
    coor_y.Normalize();
    ref->p = Vector(0, 0, 0);
    ref->M = Rotation(coor_x, coor_y, coor_z);
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::SetJointLimit(const JntArray *limit_h, const JntArray *limit_l) {
    int32_t ret = 0;
    ret = CheckJntArray(limit_h);
    if (ret != KDL_SUCCESS) return ret;
    ret = CheckJntArray(limit_l);
    if (ret != KDL_SUCCESS) return ret;
    posit_limit_h = *limit_h;
    posit_limit_l = *limit_l;
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CheckJointLimit(const JntArray *posit) {
    int32_t i = 0;
    int32_t ret = 0;
    ret = CheckJntArray(posit);
    if (ret != KDL_SUCCESS) return ret;
    for (i = 0; i < joint_num; i++) {
        if (posit->data(i) > (posit_limit_h.data(i) + EPS_JOINT_POSIT)) return KDL_LIMIT_H_OFFSET - i;
        if (posit->data(i) < (posit_limit_l.data(i) - EPS_JOINT_POSIT)) return KDL_LIMIT_L_OFFSET - i;
    }
    return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CheckJntArray(const JntArray *Jnt) {
    if (Jnt->rows() != joint_num)
        return KDL_JNT_ARRAY_ERROR_NUM;
    else
        return KDL_SUCCESS;
}

int32_t QX_ROBOT_KDL::CheckKinematicFlag(int32_t kinematic_flag) {
    if (model == QX_SCARA_V1) {
        if (kinematic_flag == SCARA_LEFT_ARM || kinematic_flag == SCARA_RHIGT_ARM)
            ;
        else
            return KDL_KINEMATIC_FLAG_ERROR;
    } else {
        if (kinematic_flag == SCARA_LEFT_ARM || kinematic_flag == SCARA_RHIGT_ARM)
            ;
        else
            return KDL_KINEMATIC_FLAG_ERROR;
    }
    return KDL_SUCCESS;
}

std::string QX_ROBOT_KDL::GetVersion() {
    return QX_ROBOT_KDL_VERSION;
}