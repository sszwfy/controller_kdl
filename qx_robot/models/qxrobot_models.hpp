/**
 * \file qxrobot_models.hpp
 * \brief 机械臂模型
 *
 * 所有模型的机械臂算法都在这个类中
 *
 */
#ifndef QXROBOT_MODELS_H
#define QXROBOT_MODELS_H
#include <chain.hpp>
#include <frames_io.hpp>

namespace KDL {
//joint epsilon
#define EPS_JOINT_POSIT 1e-8 //关节精度
#define EPS_CART 1e-8        //笛卡尔精度

//scara机械臂姿态定义
#define SCARA_RHIGT_ARM 0     //scara右臂
#define SCARA_LEFT_ARM 1      //scara左臂
#define SCARA_SINGULAR_ARM -1 //scara奇异

//6轴机械臂姿态定义
#define A6_SINGULA_ARM -1  //6轴奇异
#define A6_WAIT_ADD_ARM -2 //待添加

//DH参数结构体
typedef struct {
    double a, d, alpha, theta;
} STRUCT_DH;

//PI
#define KIN_PI (double) 3.1415926535897932384626433832795
#define KIN_2PI (double) 6.283185307179586476925286766559

//class QxModels
class QxModels {
  public:
    STRUCT_DH dh[7];
    QxModels() {
        memset(dh, 0, sizeof(STRUCT_DH) * 7);
    }
    ~QxModels() {}
    //scara 机械臂
    Chain QxScaraV1_Creat(STRUCT_DH *dh_t);
    void QxScaraV1_CheckArm(const JntArray *joint, int *kine_flag);
    int QxScaraV1_InvKin(Chain robot, const Frame *frame, JntArray joint[2]);

    //Dobot机械臂
    Chain QxDobot_Creat(STRUCT_DH *dh_t);

  private:
    inline void NormalizeJointPosit(double *data) {
        *data = atan2(sin(*data), cos(*data));
        return;
    }
};
}

#endif
