#include <chain.hpp>
#include <frames_io.hpp>
#include "qxrobot_models.hpp"
using namespace KDL;

Chain QxModels::QxDobot_Creat(STRUCT_DH *dh_t)
{
	Chain robot;
	dh[0].a = 250;  dh[0].alpha = 0; dh[0].d = 0; dh[0].theta = 0;
	dh[1].a = 150;  dh[1].alpha = 0; dh[1].d = 0; dh[1].theta = 0;
	dh[2].a = 0;	dh[2].alpha = 0; dh[2].d = 0; dh[2].theta = 0;
	dh[3].a = 0;	dh[3].alpha = 0; dh[3].d = 0; dh[3].theta = 0;
	robot.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[0].a, dh[0].alpha, dh[0].d, dh[0].theta),
		RigidBodyInertia(0, Vector::Zero(), RotationalInertia(0, 0.35, 0, 0, 0, 0))));
	robot.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[1].a, dh[1].alpha, dh[1].d, dh[1].theta),
		RigidBodyInertia(0, Vector::Zero(), RotationalInertia(0, 0.35, 0, 0, 0, 0))));
	robot.addSegment(Segment(Joint(Joint::TransZ),
		Frame::DH(dh[2].a, dh[2].alpha, dh[2].d, dh[2].theta),
		RigidBodyInertia(0, Vector::Zero(), RotationalInertia(0, 0.35, 0, 0, 0, 0))));
	robot.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[3].a, dh[3].alpha, dh[3].d, dh[3].theta),
		RigidBodyInertia(0, Vector::Zero(), RotationalInertia(0, 0.35, 0, 0, 0, 0))));
	return robot;
}
