#include <chain.hpp>
#include <frames_io.hpp>
#include "qxrobot_models.hpp"
using namespace KDL;

Chain QxModels::QxScaraV1_Creat(STRUCT_DH *dh_t)
{
    Chain qx_scara_v1;
	if(dh_t == NULL){
		dh[0].a = 250;  dh[0].alpha = 0; dh[0].d = 0; dh[0].theta = 0;
		dh[1].a = 150;  dh[1].alpha = 0; dh[1].d = 0; dh[1].theta = 0;
		dh[2].a = 0;	dh[2].alpha = 0; dh[2].d = 0; dh[2].theta = 0;
		dh[3].a = 0;	dh[3].alpha = 0; dh[3].d = 0; dh[3].theta = 0;
	}
	else{
		memcpy(dh,dh_t,sizeof(STRUCT_DH)*4);
	}
	qx_scara_v1.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[0].a, dh[0].alpha, dh[0].d, dh[0].theta),
		RigidBodyInertia(0,Vector::Zero(),RotationalInertia(0,0.35,0,0,0,0))));
	qx_scara_v1.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[1].a, dh[1].alpha, dh[1].d, dh[1].theta),
		RigidBodyInertia(0,Vector::Zero(),RotationalInertia(0,0.35,0,0,0,0))));
	qx_scara_v1.addSegment(Segment(Joint(Joint::TransZ),
		Frame::DH(dh[2].a, dh[2].alpha, dh[2].d, dh[2].theta),
		RigidBodyInertia(0,Vector::Zero(),RotationalInertia(0,0.35,0,0,0,0))));
	qx_scara_v1.addSegment(Segment(Joint(Joint::RotZ),
		Frame::DH(dh[3].a, dh[3].alpha, dh[3].d, dh[3].theta),
		RigidBodyInertia(0,Vector::Zero(),RotationalInertia(0,0.35,0,0,0,0))));
    return qx_scara_v1;
}

void QxModels::QxScaraV1_CheckArm(const JntArray *joint, int *kine_flag)
{
	if (joint->data(1) > EPS_JOINT_POSIT) *kine_flag = SCARA_RHIGT_ARM;
	else if (joint->data(1) < -EPS_JOINT_POSIT) *kine_flag = SCARA_LEFT_ARM;
	else *kine_flag = SCARA_SINGULAR_ARM;
	return;
}

int QxModels::QxScaraV1_InvKin(Chain robot, const Frame *frame, JntArray joint[2])
{
	double px = frame->p.x();
	double py = frame->p.y();
	double pz = frame->p.z();
	double roll, patch, yaw;
	frame->M.GetRPY(roll, patch, yaw);
	double R = sqrt(px*px + py*py);
	if (R > (dh[0].a + dh[1].a)+EPS_CART) return -1;
	else if (R < (dh[0].a - dh[1].a-EPS_CART)) return -1;
	double L1 = fabs(dh[0].d);
	double L2 = fabs(dh[0].a);
	double L3 = fabs(dh[1].a);
	double temp0 = (R*R + L2*L2 - L3*L3) / (2 * R*L2);
	if (temp0 > 1) temp0 = 1;
	else if (temp0 < -1) temp0 = -1;
	double Q = fabs(acos(temp0));
	double SAQ = atan2(py, px);
	temp0 = (R*R - L2*L2 - L3*L3) / (2 * L2*L3);
	if (temp0 > 1) temp0 = 1;
	else if (temp0 < -1) temp0 = -1;
	double E = fabs(acos(temp0));
	double r0[4], r1[4];
	r0[0] = SAQ - Q;
	NormalizeJointPosit(&r0[0]);
	r1[0] = SAQ + Q;
	NormalizeJointPosit(&r1[0]);
	r0[1] = E;
	r1[1] = -E;
	r0[2] = pz - L1;
	r1[2] = pz - L1;
	r0[3] = yaw - r0[0] - r0[1];
	NormalizeJointPosit(&r0[3]);
	r1[3] = yaw - r1[0] - r1[1];
	NormalizeJointPosit(&r1[3]);
	for (int i = 0; i < 4; i++){
		joint[SCARA_RHIGT_ARM].data(i) = r0[i];
		joint[SCARA_LEFT_ARM].data(i) = r1[i];
	}
	return 0;
}

