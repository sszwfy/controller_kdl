﻿#include "qxrobot_kdl.hpp"
#include <chain.hpp>
#include <chainfksolverpos_recursive.hpp>
#include <chainidsolver_recursive_newton_euler.hpp>
#include <frames_io.hpp>
#include <kinfam_io.hpp>
#include <stdio.h>

using namespace KDL;
using namespace std;

//测试机械臂的运动学正逆解
void test_fk_ik_hand(QX_ROBOT_KDL qx_kdl) {
    JntArray q(qx_kdl.GetJointNum());
    JntArray iq(qx_kdl.GetJointNum());
    q.data << -0.629307, 0.9558572, 0, 1.251747;
    Frame frame;
    RPY rpy;
    Quaternion quat;
    int fk_flag;
    qx_kdl.FkJointArray2FrameHand(&q, &frame, &fk_flag);
    qx_kdl.Frame2Quaternion(&frame, &quat);
    qx_kdl.Frame2Rpy(&frame, &rpy);
    cout << "frame p = " << frame.p << endl;
    cout << "frame rpy = " << rpy.roll << " " << rpy.pitch << " "
         << rpy.yaw << endl;
    qx_kdl.IkFrame2JointArrayHand(NULL, &frame, 0, &iq);
    printf("iq_right = %f %f %f %f \n",
           iq.data(0), iq.data(1), iq.data(2), iq.data(3));
    qx_kdl.IkFrame2JointArrayHand(NULL, &frame, 1, &iq);
    printf("iq_left = %f %f %f %f \n",
           iq.data(0), iq.data(1), iq.data(2), iq.data(3));
}

void test_fk_ik_end_effector(QX_ROBOT_KDL qx_kdl) {
    Frame eef;
    eef.p = Vector(65, 0, 0);
    eef.M = Rotation::RPY(0, 0, 0);
    qx_kdl.SetEndEffector(&eef);
    JntArray q(qx_kdl.GetJointNum());
    JntArray iq(qx_kdl.GetJointNum());
    q.data << 1, 0.5, 0, 0;
    Frame frame;
    Frame frame_right_arm, frame_left_arm;
    RPY rpy;
    Quaternion quat;
    printf("q = %f %f %f %f\n", q.data[0], q.data[1], q.data[2], q.data[3]);
    int fk_flag;
    int i = 0;
    int ret = 0;
    qx_kdl.FkJointArray2FrameEndEffector(&q, &frame, &fk_flag);
    qx_kdl.Frame2Quaternion(&frame, &quat);
    qx_kdl.Frame2Rpy(&frame, &rpy);
    cout << "frame p = " << frame.p << endl;
    cout << "frame rpy = " << rpy.roll << " " << rpy.pitch << " "
         << rpy.yaw << endl;
    while (i < 100000) {
        i++;
        q.data[0] = (double) ((rand() % 10000 - 5000)) / 10000.0 * 2.8;
        q.data[1] = (double) ((rand() % 10000 - 5000)) / 10000.0 * 2.6;
        q.data[2] = (double) ((rand() % 10000)) / 10000.0 * 100;
        q.data[3] = (double) ((rand() % 10000 - 5000)) / 10000.0 * 3.14;
        printf("q = %f %f %f %f\n", q.data[0], q.data[1], q.data[2], q.data[3]);
        qx_kdl.FkJointArray2FrameEndEffector(&q, &frame, &fk_flag);
        qx_kdl.Frame2Quaternion(&frame, &quat);
        qx_kdl.Frame2Rpy(&frame, &rpy);
        cout << "frame p = " << frame.p << endl;
        cout << "frame rpy = " << rpy.roll << " " << rpy.pitch << " "
             << rpy.yaw << endl;
        ret = qx_kdl.IkFrame2JointArrayEndEffector(NULL, &frame, SCARA_RHIGT_ARM, &iq);
        if (ret != KDL_SUCCESS) {
            printf("-------------error kdl---------------------%d\n", ret);
            return;
        }
        printf("iq_right = %f %f %f %f \n",
               iq.data(0), iq.data(1), iq.data(2), iq.data(3));
        qx_kdl.FkJointArray2FrameEndEffector(&iq, &frame_right_arm, NULL);
        ret = qx_kdl.IkFrame2JointArrayEndEffector(NULL, &frame, SCARA_LEFT_ARM, &iq);
        if (ret != KDL_SUCCESS) {
            printf("----------------error kdl1-----------------%d\n", ret);
            return;
        }
        printf("iq_left = %f %f %f %f \n",
               iq.data(0), iq.data(1), iq.data(2), iq.data(3));
        qx_kdl.FkJointArray2FrameEndEffector(&iq, &frame_left_arm, NULL);
        if (frame != frame_right_arm) {
            printf("-------------------error frame_right_arm-------------------\n");
            return;
        }
        if (frame != frame_left_arm) {
            printf("-------------------error frame_left_arm-------------------\n");
            return;
        }
        printf("i = %d====================================================\n\n", i);
    }
    return;
}

//测试参考坐标系
void test_ref_frame(QX_ROBOT_KDL qx_kdl, Frame ref_pose, Frame frame_ref) {
    RPY rpy;
    qx_kdl.Frame2Rpy(&ref_pose, &rpy);
    printf("ref_pose.P = %f %f %f RPY = %f %f %f\n",
           ref_pose.p.x(), ref_pose.p.y(), ref_pose.p.z(),
           rpy.roll, rpy.pitch, rpy.yaw);
    qx_kdl.Frame2Rpy(&frame_ref, &rpy);
    printf("frame_ref.P = %f %f %f RPY=%f %f %f\n",
           frame_ref.p.x(), frame_ref.p.y(), frame_ref.p.z(),
           rpy.roll, rpy.pitch, rpy.yaw);
    cout << "frame_ref.M = " << endl
         << frame_ref.M << endl;

    //将参考坐标系位置转换到世界坐标系
    Frame world_pose = qx_kdl.RefPose2World(&ref_pose, &frame_ref);
    qx_kdl.Frame2Rpy(&world_pose, &rpy);
    printf("frame_world.P = %f %f %f RPY = %f %f %f\n",
           world_pose.p.x(), world_pose.p.y(), world_pose.p.z(),
           rpy.roll, rpy.pitch, rpy.yaw);
    //将世界坐标系位置转换到参考坐标系
    Frame calc_ref_pose = qx_kdl.WorldPose2Ref(&world_pose, &frame_ref);
    qx_kdl.Frame2Rpy(&calc_ref_pose, &rpy);
    printf("calc_ref_pose.P = %f %f %f RPY = %f %f %f\n",
           calc_ref_pose.p.x(), calc_ref_pose.p.y(), calc_ref_pose.p.z(),
           rpy.roll, rpy.pitch, rpy.yaw);
    printf("----------------------------------\n\n");
    return;
}

//测试单点示教参考坐标系
void test_teach_one_point_ref(QX_ROBOT_KDL qx_kdl) {
    printf("---------test_teach_one_point_ref------------\n");
    Frame OnePoint1;
    //第一个点的坐标
    OnePoint1.M = Rotation::RPY(1, 1, 1);
    OnePoint1.p = Vector(-100, 20, 10);
    //计算frame_ref
    Frame frame_ref;
    qx_kdl.CalcRefOnePoint(&frame_ref, &OnePoint1);
    Frame ref_pose;
    ref_pose.M = Rotation::RPY(0, 0, 0);
    ref_pose.p = Vector(0, 1, 1);
    test_ref_frame(qx_kdl, ref_pose, frame_ref);
    return;
}

//测试两点示教参考坐标系 带原点
void test_teach_2point_with_origin_ref(QX_ROBOT_KDL qx_kdl) {
    printf("---------test_teach_2point_with_origin_ref------------\n");
    Frame point1, point2;
    //第一个点的坐标(原点)
    point1.M = Rotation::RPY(0, 0, 0);
    point1.p = Vector(200, -200, 0);
    //第二个点的坐标
    point2.M = Rotation::RPY(0, 0, 0);
    point2.p = Vector(200, 200, 0);
    Frame frame_ref;
    //生成参考坐标系
    qx_kdl.CalcRefTwoPointWithOrigin(&frame_ref, &point1, &point2);
    //测试参考坐标系
    Frame ref_pose;
    ref_pose.M = Rotation::RPY(0, 0, PI);
    ref_pose.p = Vector(100, 0, 0);
    test_ref_frame(qx_kdl, ref_pose, frame_ref);
    return;
}

//测试两点示教参考坐标系 无原点
void test_teach_2point_no_origin_ref(QX_ROBOT_KDL qx_kdl) {
    printf("---------test_teach_2point_no_origin_ref------------\n");
    Frame point1, point2;
    Frame ref_pose;
    Frame frame_ref;
    //第一个点的坐标
    point1.M = Rotation::RPY(0, 0, 0);
    point1.p = Vector(200, -200, 0);
    //第二个点的坐标
    point2.M = Rotation::RPY(0, 0, 0);
    point2.p = Vector(200, 100, 0);
    //生成参考坐标系
    qx_kdl.CalcRefTwoPointNoOrigin(&frame_ref, &point1, &point2);
    //测试参考坐标系
    ref_pose.M = Rotation::RPY(0, 0, 0);
    ref_pose.p = Vector(100, 0, 0);
    test_ref_frame(qx_kdl, ref_pose, frame_ref);
    return;
}

//测试三点示教参考坐标系 带原点
void test_teach_3point_with_origin_ref(QX_ROBOT_KDL qx_kdl) {
    printf("ith_origin_ref------------\n");
    Frame point1, point2, point3;
    Frame ref_pose;
    Frame frame_ref;
    int ret = 0;
    //第一个点的坐标(原点)
    point1.M = Rotation::RPY(1, 1, 1);
    point1.p = Vector(100, 100, 20);
    //第二个点的坐标
    point2.M = Rotation::RPY(2, 2, 2);
    point2.p = Vector(100, 200, 30);
    //第三个点的坐标
    point3.M = Rotation::RPY(3, 3, 3);
    point3.p = Vector(123, 123, 50);
    //生成参考坐标系
    ret = qx_kdl.CalcRefThreePointWithOrigin(&frame_ref, &point1, &point2, &point3);
    RPY rpy_t;
    qx_kdl.Frame2Rpy(&frame_ref, &rpy_t);
    if (ret != 0) {
        printf("test_teach_2point_with_origin_ref error,ret = %d\n", ret);
        return;
    }
    //测试参考坐标系
    ref_pose.M = Rotation::RPY(0, 0, 0);
    ref_pose.p = Vector(0, 0, 0);
    test_ref_frame(qx_kdl, ref_pose, frame_ref);
    return;
}

void test_temp(QX_ROBOT_KDL &qx_kdl) {
    Frame frame_input;
    Frame frame_check;
    int32_t fk_flag, ik_flag = 0;
    Frame frame_eef;
    frame_eef.M = Rotation::RPY(0, 0, 0);
    frame_eef.p = Vector(65, 0, 0);
    qx_kdl.SetEndEffector(&frame_eef);
    JntArray iq(qx_kdl.GetJointNum());
    JntArray q(qx_kdl.GetJointNum());
    //frame_input.M = Rotation::RPY(0, 0, 0);
    //frame_input.p = Vector(347.978, -30.407, 0);
    //q.data << -0.732663, 1.2572498, 0, 0.569875;
    qx_kdl.FkJointArray2FrameEndEffector(&q, &frame_input, NULL);
    double r, p, y;
    frame_input.M.GetRPY(r, p, y);
    cout << "frame_input = " << frame_input.p << r << p << y << endl;
    int ret = qx_kdl.IkFrame2JointArrayHand(NULL, &frame_input, ik_flag, &iq);
    if (ret != KDL_SUCCESS) {
        printf("inverse kinematics faild ret = %d\n", ret);
    } else {
        printf("iq_left = %f %f %f %f \n",
               iq.data(0), iq.data(1), iq.data(2), iq.data(3));
        qx_kdl.FkJointArray2FrameHand(&iq, &frame_check, &fk_flag);
        if (fk_flag != ik_flag) {
            printf("kinemtic flag error\n");
        }
        if (frame_input != frame_check) {
            printf("frame check error\n");
        }
        printf("check done\n");
    }
    return;
}

void test_rotation() {
    Rotation rot1 = Rotation::RPY(0.5, 0, 0);
    Rotation rot2 = Rotation::RPY(0.1, 0.1, 0.1);
    Rotation rot4 = Rotation::RPY(-0.1, -0.1, -0.1);
    Rotation rot3 = Rotation::RPY(0.5, PI / 2, 0.5);
    double r, p, y;
    rot3.GetRPY(r, p, y);
    return;
}

int main() {
    QX_ROBOT_KDL qx_kdl(QX_SCARA_V1);
    string Version = qx_kdl.GetVersion();
    cout << "******** version = " << Version << "*********" << endl;
    //test_rotation();
    //test_temp(qx_kdl);
    //test_fk_ik_hand(qx_kdl);
    // test_fk_ik_end_effector(qx_kdl);

    //==========================================

    // Frame ref_pose, frame_ref;
    // //参考坐标系位姿
    // ref_pose.M = Rotation::RPY(0, 0, PI / 2);
    // ref_pose.p = Vector(350, 0, 0);
    // //坐标转换frame
    // frame_ref.M = Rotation::RPY(0, 0, 0);
    // frame_ref.p = Vector(0, 0, 0);
    // test_ref_frame(qx_kdl, ref_pose, frame_ref);

    //==========================================
    // test_teach_one_point_ref(qx_kdl);
    // test_teach_2point_with_origin_ref(qx_kdl);
    // test_teach_2point_no_origin_ref(qx_kdl);
    test_teach_3point_with_origin_ref(qx_kdl);

    //==========================================
    printf("++++++++++++++++++++++++++++\n");
    return 0;
}
