琦星机器人KDL库说明
============
> 目录
<!-- TOC -->

- [1. 修改记录](#1-修改记录)
    - [1.1. 2017-6-19](#11-2017-6-19)
    - [1.2. 2017-9-3](#12-2017-9-3)
- [2. 功能需求](#2-功能需求)
    - [2.1. 需求2017-6-19](#21-需求2017-6-19)
- [3. 接口说明](#3-接口说明)
    - [3.1. UI层](#31-ui层)
    - [3.2. 主控制器层](#32-主控制器层)
- [4. 宏定义、变量等说明](#4-宏定义变量等说明)
    - [4.1. 机械臂类型](#41-机械臂类型)
    - [4.2. 关于fk_flag和ik_flag](#42-关于fk_flag和ik_flag)
        - [4.2.1. scara机械臂姿态](#421-scara机械臂姿态)
        - [4.2.2. 6轴机械臂姿态](#422-6轴机械臂姿态)
- [5. API说明](#5-api说明)
    - [5.1. 初始化](#51-初始化)
    - [5.2. 运动学算法](#52-运动学算法)
        - [5.2.1. 运动学手掌正解](#521-运动学手掌正解)
        - [5.2.2. 运动学手掌逆解](#522-运动学手掌逆解)
        - [5.2.3. 设置EEF](#523-设置eef)
        - [5.2.4. 运动学EEF正解](#524-运动学eef正解)
        - [5.2.5. 运动学EEF逆解](#525-运动学eef逆解)
    - [5.3. 基本函数](#53-基本函数)
        - [5.3.1. 获取关节数量](#531-获取关节数量)
        - [5.3.2. 获取版本号](#532-获取版本号)
    - [5.4. 坐标系标定、转换](#54-坐标系标定转换)
        - [5.4.1. 参考坐标到世界坐标系](#541-参考坐标到世界坐标系)
        - [5.4.2. 世界坐标系到参考坐标系](#542-世界坐标系到参考坐标系)
        - [5.4.3. 一点坐标系标定](#543-一点坐标系标定)
        - [5.4.4. 三点坐标系标定(含原点)](#544-三点坐标系标定含原点)
        - [5.4.5. 三点坐标系标定(无原点)](#545-三点坐标系标定无原点)
        - [5.4.6. 两点坐标系标定(无原点)](#546-两点坐标系标定无原点)
        - [5.4.7. 两点坐标系标定(含原点)](#547-两点坐标系标定含原点)
- [6. 示例](#6-示例)
    - [6.1. 示例1](#61-示例1)

<!-- /TOC -->

# 1. 修改记录
* 请在此处记录每次修改此文档的大致内容
* 当前KDL版本：0.1.0
* 当前需求：需求2017-6-19

## 1.1. 2017-6-19
1. 初始版本

## 1.2. 2017-9-3
1. 增加参考坐标系的测试结果，供使用参考

# 2. 功能需求
* 目的：向UI软件和控制器提供动力学、动力学、几何变换的相关算法库

## 2.1. 需求2017-6-19
1. 机械臂运动学的正逆解算法
2. 参考坐标系标定方法，包括：一点、三点含原点、三点无原点、两点无原点、两点带原点的标定方法
3. 提供参考坐标系的转换函数
4. 提供姿态转换相关函数
5. 转换世界坐标系位置到参考坐标系
6. 对应KDL版本0.1.0

# 3. 接口说明
## 3.1. UI层
* 提供windows环境下的,动态库和静态库

## 3.2. 主控制器层
* 提供linux环境下,arm和x86的动静态库

# 4. 宏定义、变量等说明
## 4.1. 机械臂类型
* 定义了机械臂的各种类型
* 包括QX_SCARA_V1、QX_A6_V1
* 源码
    ```c
    typedef enum{
		QX_SCARA_V1 = 0,    //scara机械臂
		QX_A6_V1 = 1,       //6轴机械臂
	}QX_ROBOT_MODELS;
    ```    
    
## 4.2. 关于fk_flag和ik_flag
* 机械臂的姿态
### 4.2.1. scara机械臂姿态
* scara机械臂包括左臂、右臂、奇异三种姿态
* 源码
    ```c
    //scara机械臂姿态定义
    #define SCARA_RHIGT_ARM 0			//scara右臂
    #define SCARA_LEFT_ARM 1			//scara左臂
    #define SCARA_SINGULAR_ARM -1		//scara奇异
    ```
### 4.2.2. 6轴机械臂姿态
* 6轴机械臂包括8种姿态描述,待添加
* 源码
    ```c
    //6轴机械臂姿态定义
    #define A6_SINGULA_ARM -1			//6轴奇异
    #define A6_WAIT_ADD_ARM -2			//待添加
    ```

# 5. API说明
## 5.1. 初始化
* 函数名称：`QX_ROBOT_KDL(QX_ROBOT_MODELS model_t);`
* 参数：`QX_ROBOT_MODELS model_t`: 机械臂类型,参考 [3.1. 机械臂类型](#31-机械臂类型)
* 返回值：void
* 备注：初始化kdl算法模块
* 示例
```c
    //创建KDL算法类
    QX_ROBOT_KDL qx_kdl(QX_SCARA_V1);
```

## 5.2. 运动学算法
### 5.2.1. 运动学手掌正解
* 函数名称：`int FkJointArray2FrameHand(const JntArray *joint, Frame *frame, int *fk_flag);`
* 参数：
    - `const JntArray *joint`: 关节位置
    - `Frame *frame`: 正解的结果
    - `int *fk_flag`: 正解姿态描述，参考 [3.2. 关于fk_flag和ik_flag](#32-关于fk_flag和ik_flag)
* 返回值：
    - 0：成功
    - 其他值：失败
* 备注：
* 示例:

### 5.2.2. 运动学手掌逆解
* 函数名称：`int IkFrame2JointArrayHand(const JntArray *last_posit, const Frame *frame, int ik_flag, JntArray *joint);`
* 参数：
    - `const JntArray *last_posit`: 上次的位置,可以为NULL,（备用给6轴解决关节内部奇异）
    - `const Frame *frame`: 手掌末端位姿
    - `int ik_flag`：逆解姿态描述，参考 [3.2. 关于fk_flag和ik_flag](#32-关于fk_flag和ik_flag)
    - `JntArray *joint`: 逆解结果
* 返回值：
    - 0：success
    - -1：ik_flag错误
    - -2：逆解失败,不在工作范围内
* 备注：
* 示例

### 5.2.3. 设置EEF
* 函数名称：`int SetEndEffector(const Frame *frame);`
* 参数：
    - `const Frame *frame`: end effector 描述
* 返回值：
    - 0：success
* 备注：
* 示例

### 5.2.4. 运动学EEF正解
* 函数名称：`int FkJointArray2FrameEndEffector(const JntArray *joint, Frame *frame, int *fk_flag);`
* 参数：
    - `const Frame *frame`: 手掌末端位姿
    - `int ik_flag`：逆解姿态描述，参考 [3.2. 关于fk_flag和ik_flag](#32-关于fk_flag和ik_flag)
    - `JntArray *joint`: 逆解结果
* 返回值：
    - 0：success
* 备注：
* 示例

### 5.2.5. 运动学EEF逆解
* 函数名称：`int IkFrame2JointArrayEndEffector(const JntArray *last_posit, const Frame *frame, int ik_flag, JntArray *joint);`
* 参数：
    - `const JntArray *last_posit`: 上次的位置,可以为NULL,（备用给6轴解决关节内部奇异）
    - `const Frame *frame`: end effector 末端位姿
    - `int ik_flag`：逆解姿态描述，参考 [3.2. 关于fk_flag和ik_flag](#32-关于fk_flag和ik_flag)
    - `JntArray *joint`: 逆解结果
* 返回值：
    - 0：success
    - -1：ik_flag错误
    - -2：逆解失败,不在工作范围内
* 备注：
* 示例

## 5.3. 基本函数
### 5.3.1. 获取关节数量
* 函数名称：`unsigned int GetJointNum();`
* 参数：`viod`
* 返回值：
    - `unsigned int`关节数量
* 备注：
* 示例

### 5.3.2. 获取版本号
* 函数名称：`unsigned int GetJointNum();`
* 参数：`viod`
* 返回值：
    - `unsigned int`关节数量
* 备注：
* 示例

## 5.4. 坐标系标定、转换
### 5.4.1. 参考坐标到世界坐标系
* 函数名称：`Frame RefPose2World(const Frame *ref_pose, const Frame *ref_frame);`
* 参数：
    - `const Frame *ref_pose`: 参考坐标系位姿
    - `const Frame *ref_frame`: 参考坐标系到世界坐标系转换矩阵
* 返回值：
    - `Frame`世界坐标系位姿
* 备注：
* 示例

### 5.4.2. 世界坐标系到参考坐标系
* 函数名称：`Frame WorldPose2Ref(const Frame *world_pose, const Frame *ref_frame);`
* 参数：
    - `const Frame *world_pose`: 世界坐标系位置
    - `const Frame *ref_frame`: 参考坐标系到世界坐标系转换矩阵
* 返回值：
    - `Frame`参考坐标系位姿
* 备注：
* 示例

### 5.4.3. 一点坐标系标定
* 标定方法<div align=center> 
    ![机械臂功能](./images/单点示教坐标系.png) </div>
    <p align="center">(图4.1 单点示教坐标系)</p>
* 函数名称：`int CalcRefOnePoint(Frame *ref, const Frame *point1);`
* 参数：
    - `Frame *ref`: 计算结果
    - `const Frame *point1`: 标定点1
* 返回值：
    - 0：计算成功
* 备注：
* 示例：
    | 参数 | x  | y  | z  |  roll  |  pitch  | yaw|
    | ----|:--:|---:| ---:|--:|---:| --:|
    参考点| 200 | 0 | 0 | 0 | 0 | 0 |
    结果 | 200 | 0 | 0 | 0 | 0| 0|

### 5.4.4. 三点坐标系标定(含原点)
* 标定方法<div align=center> 
    ![机械臂功能](./images/三点带原点示教坐标系.png) </div>
    <p align="center">(图4.2 三点带原点示教坐标系)</p>
* 函数名称：`int CalcRefThreePointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3);`
* 参数：
    - `Frame *ref`: 计算结果
    - `const Frame *point1`: 标定点1,原点
    - `const Frame *point2`: 标定点2
    - `const Frame *point3`: 标定点3
* 返回值：
    - 0：成功
    - -1：point1到point2距离太小，无法标定
    - -2：point1到point3距离太小，无法标定
    - -3：三点接近共线，无法标定
* 备注：
* 示例

### 5.4.5. 三点坐标系标定(无原点)
* 标定方法<div align=center> 
    ![机械臂功能](./images/三点无原点示教坐标系.png) </div>
    <p align="center">(图4.3 三点坐标系标定无原点)</p>
* 函数名称：`int CalcRefThreePointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2, const Frame *point3);`
* 参数：
    - `Frame *ref`: 计算结果
    - `const Frame *point1`: 标定点1
    - `const Frame *point2`: 标定点2，无法标定
    - `const Frame *point3`: 标定点3，无法标定
* 返回值：
    -  0：成功
    - -1：point1到point2距离太小
    - -2：point1到point3距离太小
    - -3：三点接近共线
* 备注：
* 示例

### 5.4.6. 两点坐标系标定(无原点)
* 函数名称：`int CalcRefTwoPointNoOrigin(Frame *ref, const Frame *point1, const Frame *point2);`
* 参数：
    - `Frame *ref`: 计算结果
    - `const Frame *point1`: 标定点1
    - ` const Frame *point2`: 标定点2
* 返回值：
    - 0：标定成功
    - -1：标定失败，两点位置太接近
* 备注：
    1. 两点无原点标定方法用于标定参考坐标系的旋转
    2. 参考坐标系原点还是世界坐标系原点
* 示例
    | 参数 | x  | y  | z  |  roll  |  pitch  | yaw|
    | ----|:--:|---:| ---:|--:|---:| --:|
    参考点1| 200 | -200 | 0 | 0 | 0 | 0 |
    参考点2 | 200 | 100 | 0 | 0 | 0| 0 |
    结果 | 0 | 0 | 0 | 0 | 0 | 1.570796 |
    
### 5.4.7. 两点坐标系标定(含原点)
* 标定方法<div align=center> 
    ![机械臂功能](./images/两点示教带原点.jpg) </div>
    <p align="center">(图4.3 两点示教带原点)</p>
* 函数名称：`int CalcRefTwoPointWithOrigin(Frame *ref, const Frame *point1, const Frame *point2);`
* 参数：
    - `Frame *ref`: 计算结果
    - `const Frame *point1`: 标定点1，参考坐标系原点
    - ` const Frame *point2`: 标定点2
* 返回值：
    - 0：标定成功
    - -1：标定失败，两点位置太接近
* 备注：
* 示例
    | 参数 | x  | y  | z  |  roll  |  pitch  | yaw|
    | ----|:--:|---:| ---:|--:|---:| --:|
    参考点1| 200 | -200 | 0 | 0 | 0 | 0 |
    参考点2 | 200 | 100 | 0 | 0 | 0| 0|
    结果 | 200 | -200 | 0 | 0 | 0 | 1.570796 |
    
# 6. 示例
## 6.1. 示例1
* 目录：TEST
* 编译过程
	- 1. 进入TEST目录，创建文件夹build
	- 2. windows下执行cmake,添加变量ARCH,类型为string,数据为WINDOWS
	- 3. arm linux下执行cmake .. -DARCH=LINUX_ARM
	- 4. x86 linux下执行cmake .. -DARCH=LINUX_X86
	- 5. make

